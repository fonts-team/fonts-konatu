fonts-konatu (20121218-11) unstable; urgency=medium

  * Drop debian/fonts-konatu.lintian-overrides, now lintian doesn't report
    such warning anymore
  * debian/control
    - Note it as Japanese font explicitly (Closes: #962473)

 -- Hideki Yamane <henrich@debian.org>  Sat, 17 Oct 2020 22:31:33 +0900

fonts-konatu (20121218-10) unstable; urgency=medium

  * Trim trailing whitespace.
  * Update standards version to 4.5.0, no changes needed.
  * debian/control
    - drop unnecessary dependencies for old ttf-konatu
    - drop debhelper and bump debhelper-compat (= 13)

 -- Hideki Yamane <henrich@debian.org>  Wed, 06 May 2020 07:59:44 +0900

fonts-konatu (20121218-9) unstable; urgency=medium

  * debian/copyright
    - use https
    - update copyright year
  * debian/control
    - set Standards-Version: 4.3.0
    - update Vcs-* to use salsa.debian.org
  * debian/{control,compat}
    - use dh12
  * debian/rules
    - use default compression
  * drop unnecessary debian/fonts-konatu.preinst

 -- Hideki Yamane <henrich@debian.org>  Sun, 14 Apr 2019 00:20:08 +0900

fonts-konatu (20121218-8) unstable; urgency=medium

  * debian/watch
    - update URL and itself to version 4

 -- Hideki Yamane <henrich@debian.org>  Thu, 13 Jul 2017 20:05:21 +0900

fonts-konatu (20121218-7) unstable; urgency=medium

  * debian/control
    - update Homepage: field
    - set Standards-Version: 4.0.0
    - Build-Depends: debhelper (>= 10)
  * debian/compat
    - set 10
  * debian/copyright
    - update upstream Source URL

 -- Hideki Yamane <henrich@debian.org>  Tue, 11 Jul 2017 21:30:28 +0900

fonts-konatu (20121218-6) unstable; urgency=medium

  * fix regression in debian/fonts-konatu.preinst

 -- Hideki Yamane <henrich@debian.org>  Sun, 03 Jan 2016 16:37:35 +0900

fonts-konatu (20121218-5) unstable; urgency=medium

  * debian/fonts-konatu.preinst
    - fix "command-with-path-in-maintainer-script"
  * debian/control
    - update Vcs-*

 -- Hideki Yamane <henrich@debian.org>  Sun, 06 Dec 2015 14:14:35 +0900

fonts-konatu (20121218-4) unstable; urgency=medium

  * debian/control
    - set Standards-Version: 3.9.6
    - update Vcs-Browser

 -- Hideki Yamane <henrich@debian.org>  Mon, 05 Jan 2015 21:22:56 +0900

fonts-konatu (20121218-3) unstable; urgency=low

  * debian/control
    - drop ttf- package
    - use canonical URL for Vcs-*

 -- Hideki Yamane <henrich@debian.org>  Sun, 16 Jun 2013 05:21:10 +0900

fonts-konatu (20121218-2) unstable; urgency=low

  * debian/control
    - set Standards-Version: 3.9.4
    - update Vcs-* field, moved from svn to git
    - make it "Multi-Arch: foreign"
  * upload to unstable

 -- Hideki Yamane <henrich@debian.org>  Tue, 04 Jun 2013 18:28:23 +0900

fonts-konatu (20121218-1) experimental; urgency=low

  * New upstream release
  * fix debian/watch

 -- Hideki Yamane <henrich@debian.org>  Tue, 18 Dec 2012 19:31:49 +0900

fonts-konatu (201212-1) experimental; urgency=low

  * New upstream release
  * update debian/watch to deal with upstream change
  * debian/copyright
    - use copyright format 1.0
    - update upstream license as MIT

 -- Hideki Yamane <henrich@debian.org>  Tue, 18 Dec 2012 14:33:47 +0900

fonts-konatu (26-9) unstable; urgency=low

  * debian/control
    - add "Pre-Depends: dpkg (>= 1.15.6~)"
    - bump up debhelper version to deal with xz option
    - replace s/Conflicts/Breaks/ to more Policy polite.
  * debian/compat
    - set 9
  * debian/fonts-konatu.install,lintian*
    - remove unused override
  * debian/fonts-konatu.lintian-overrides
    - add to override "no-upstream-changelog" because upstream provides
      changelog as README_Japanese.txt

 -- Hideki Yamane <henrich@debian.org>  Tue, 12 Jun 2012 22:45:35 +0900

fonts-konatu (26-8) unstable; urgency=low

  * debian/rules
    - compress with xz

 -- Hideki Yamane <henrich@debian.org>  Sun, 03 Jun 2012 20:02:15 +0900

fonts-konatu (26-7) unstable; urgency=low

  * debian/control
    - set "Standards-Version: 3.9.3"
    - add "Priority: extra" to transitional package

 -- Hideki Yamane <henrich@debian.org>  Sat, 10 Mar 2012 05:27:19 +0900

fonts-konatu (26-6) unstable; urgency=low

  * debian/fonts-konatu.install
    - revert change for font install path

 -- Hideki Yamane <henrich@debian.org>  Tue, 27 Sep 2011 23:23:07 +0900

fonts-konatu (26-5) unstable; urgency=low

  * debian/control
    - introduce ttf-konatu as transitional dummy package
    - specify version for Conflicts: and Replaces: , and remove Provides:
    - fix Vcs-Svn

 -- Hideki Yamane <henrich@debian.org>  Sat, 30 Jul 2011 18:24:33 +0200

fonts-konatu (26-4) unstable; urgency=low

  * debian/control
    - remove "DM-Upload-Allowed: yes" since now it is not necessary.
  * add lintian overrides to ignore fonts-duplicates during transition.

 -- Hideki Yamane <henrich@debian.org>  Sun, 24 Jul 2011 12:17:09 +0200

fonts-konatu (26-3) unstable; urgency=low

  * rename to new package name
  * debian/control:
    - set "Standards-Version: 3.9.2" without changes

 -- Hideki Yamane <henrich@debian.org>  Tue, 19 Jul 2011 00:48:05 +0900

ttf-konatu (26-2) unstable; urgency=low

  * update debian/watch file

 -- Hideki Yamane (Debian-JP) <henrich@debian.or.jp>  Sat, 06 Mar 2010 01:22:53 +0900

ttf-konatu (26-1) unstable; urgency=low

  * New upstream release
  * debian/control:
    - set "Standards-Version: 3.8.4" without changes

 -- Hideki Yamane (Debian-JP) <henrich@debian.or.jp>  Tue, 02 Feb 2010 20:23:10 +0900

ttf-konatu (25-3) unstable; urgency=low

  * fix preinst script (set -e)
  * debian/control: drop "Suggests:" field.

 -- Hideki Yamane (Debian-JP) <henrich@debian.or.jp>  Fri, 18 Dec 2009 09:52:38 +0900

ttf-konatu (25-2) unstable; urgency=low

  * Switch to debhelper7
  * Switch to source format version 3.0
  * Dropping defoma

 -- Hideki Yamane (Debian-JP) <henrich@debian.or.jp>  Fri, 11 Dec 2009 09:06:06 +0900

ttf-konatu (25-1) unstable; urgency=low

  * New upstream release
  * debian/control:
    - Update to Standards-Version: 3.8.3 with no change

 -- Hideki Yamane (Debian-JP) <henrich@debian.or.jp>  Mon, 21 Sep 2009 00:35:44 +0900

ttf-konatu (24-3) unstable; urgency=low

  * rebuild to build postinst and prerm scripts correctly.

 -- Hideki Yamane (Debian-JP) <henrich@debian.or.jp>  Thu, 25 Jun 2009 20:20:29 +0900

ttf-konatu (24-2) unstable; urgency=low

  * debian/prerm
    - remove fc-cache
  * debian/rules
    - clean it. remove unneccessary lines.

 -- Hideki Yamane (Debian-JP) <henrich@debian.or.jp>  Wed, 24 Jun 2009 22:30:12 +0900

ttf-konatu (24-1) unstable; urgency=low

  * New upstream release
  * debian/control:
    - Update to Standards-Version: 3.8.2 with no change
    - Suggests: fontconfig (>= 2.6.0-4)
  * debian/postinst:
    - remove fc-cache
  * debian/watch:
    - fix it.

 -- Hideki Yamane (Debian-JP) <henrich@debian.or.jp>  Thu, 18 Jun 2009 03:03:55 +0900

ttf-konatu (23-3) unstable; urgency=low

  * debian/control:
    - new sections "fonts"
    - Standards-Version: 3.8.1

 -- Hideki Yamane (Debian-JP) <henrich@debian.or.jp>  Wed, 25 Mar 2009 07:03:27 +0900

ttf-konatu (23-2) unstable; urgency=low

  * debian/copyright
    - fix "copyright-with-old-dh-make-debian-copyright"

 -- Hideki Yamane (Debian-JP) <henrich@debian.or.jp>  Sun, 22 Feb 2009 03:41:46 +0900

ttf-konatu (23-1) unstable; urgency=low

  * New upstream release (Closes: #512904)

 -- Hideki Yamane (Debian-JP) <henrich@debian.or.jp>  Sun, 25 Jan 2009 12:12:52 +0900

ttf-konatu (22-5) unstable; urgency=low

  * debian/{postinst,prerm}
    - fix lintian warning: command-with-path-in-maintainer-script

 -- Hideki Yamane (Debian-JP) <henrich@debian.or.jp>  Sat, 13 Sep 2008 21:55:20 +0900

ttf-konatu (22-4) unstable; urgency=low

  * debian/control
    - Bump up Standards-Version: 3.8.0 with no change.
    - set Priority: optional

 -- Hideki Yamane (Debian-JP) <henrich@debian.or.jp>  Fri, 20 Jun 2008 01:22:27 +0900

ttf-konatu (22-3) unstable; urgency=low

  * debian/control
    - use Build-Depends-Indep: field for defoma and bzip2, not
      Build-Depends:. Thanks to lintian.
    - set priority is extra, not optional.
    - Suggests: xfs (>= 1:1.0.1-5).
    - add "DM-Upload-Allowed: yes"

 -- Hideki Yamane (Debian-JP) <henrich@debian.or.jp>  Sat, 19 Apr 2008 09:27:59 +0900

ttf-konatu (22-2) unstable; urgency=low

  * debian/control
    - set Debian Fonts Task Force <pkg-fonts-devel@lists.alioth.debian.org>
      as maintainer, and me as Uploaders.
    - add Vcs-Svn: and Vcs-Browser: field.

 -- Hideki Yamane (Debian-JP) <henrich@debian.or.jp>  Tue, 05 Feb 2008 18:34:21 +0900

ttf-konatu (22-1) unstable; urgency=low

  * New upstream release.
  * debian/control
    - Standard-Version: 3.7.3, and fix "Homepage:" field (Closes: #448131).
  * debian/rules
    - fix "binary-arch-rules-but-pkg-is-arch-indep"
  * debian/watch
    - fix to work correctly. use "downloadurlmangle" option.
  * debian/copyright
    - change whole package license under CC3.0 by-SA.

 -- Hideki Yamane (Debian-JP) <henrich@debian.or.jp>  Sun, 16 Dec 2007 11:06:54 +0900

ttf-konatu (21-1) unstable; urgency=low

  * New upstream release.

 -- Hideki Yamane (Debian-JP) <henrich@debian.or.jp>  Wed, 07 Nov 2007 15:33:35 +0900

ttf-konatu (20-1) unstable; urgency=low

  * Upload to Official Debian repository (closes: #430339) .

 -- Hideki Yamane (Debian-JP) <henrich@debian.or.jp>  Sat, 30 Jun 2007 13:07:06 +0900

ttf-konatu (20-0+private3) unstable; urgency=low

  * debian/prerm
    - remove " /usr/bin/defoma-font -vt purge-all" because its function
      would be generated with dh_defomainstall and if it exists it gets
      a trouble when package would be removed.
  * debian/rules
    - fix build problem.

 -- Hideki Yamane (Debian-JP) <henrich@debian.or.jp>  Sun, 17 Jun 2007 01:08:48 +0900

ttf-konatu (20-0+private2) unstable; urgency=low

  * debian/control
    - add missing Build-Depends: defoma. Thanks to Junich Uekawa.

 -- Hideki Yamane (Debian-JP) <henrich@debian.or.jp>  Sat, 09 Jun 2007 22:46:46 +0900

ttf-konatu (20-0+private1) unstable; urgency=low

  * New upstream release.
  * remove debian/99-ttf-konatu.conf and use dh_installdefoma in rules file.
    And move debian/defoma/ttf-konatu.hints to debian/ttf-konatu.defoma-hints.

 -- Hideki Yamane (Debian-JP) <henrich@debian.or.jp>  Sat, 09 Jun 2007 20:19:56 +0900

ttf-konatu (19-0+private3) unstable; urgency=low

  * debian/control
    - fix Depends and Build-Depends. unzip is needed for Build-Depends,
      not Depends.

 -- Hideki Yamane (Debian-JP) <henrich@debian.or.jp>  Wed, 23 May 2007 22:07:50 +0900

ttf-konatu (19-0+private2) unstable; urgency=low

  * debian/postinst
    - fix to cancel create symlink if it exists.
  * debian/control
    - tweak description.

 -- Hideki Yamane (Debian-JP) <henrich@debian.or.jp>  Sat, 19 May 2007 19:29:11 +0900

ttf-akonatu (19-0+private1) unstable; urgency=low

  * New upstream release.
  * License is changed.
    New license is Creative Commons Attribution-ShareAlike 3.0 Unported.
    Thanks to Mitiya Masuda.
  * debian/rules
    - fix to really clean files.
  * debian/watch
    - disable it.

 -- Hideki Yamane (Debian-JP) <henrich@debian.or.jp>  Fri, 18 May 2007 23:56:41 +0900

ttaf-konatu (18-0+private1) unstable; urgency=low

  * Initial release.

 -- Hideki Yamane (Debian-JP) <henrich@debian.or.jp>  Sun, 13 May 2007 19:30:24 +0900
